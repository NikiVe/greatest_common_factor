<?php


namespace GCF;


use Brick\Math\BigInteger;
use Tester\TestFunc;

/**
 * Class Euclid_2
 * @package GCF
 *
 * Бинарный алгоритм вычисления НОД
 */
class Binary implements TestFunc
{
    public function run(string $values): string
    {
        $arValues = explode(PHP_EOL, $values);

        $a = BigInteger::of($arValues[0]);
        $b = BigInteger::of($arValues[1]);

        $one = BigInteger::of(1);
        $two = BigInteger::of(2);

        $power = 0;
        while (true) {


            if ($a->isEqualTo($b)) {
                break;
            }

            // НОД(a, 0) = a
            if ($a->isZero()) {
                return $a;
            }

            // НОД(0, b) = b
            if ($b->isZero()) {
                return $b;
            }

            // НОД(a, b) = a | a = b
            if ($a->isEqualTo($b)) {
                return $b;
            }

            // НОД(1, b) = 1; НОД(a, 1) = 1;
            if ($a->isEqualTo($one) || $b->isEqualTo($one)) {
                return (string)$one;
            }

            // Если a, b чётные, то НОД(a, b) = 2*НОД(a/2, b/2);
            if ($a->isEven() && $b->isEven()) {
                $a = $a->dividedBy($two);
                $b = $b->dividedBy($two);
                $power++;
                continue;
            }

            // Если a чётное, b нечётное, то НОД(a, b) = НОД(a/2, b);
            if ($a->isEven() && $b->isOdd()) {
                $a = $a->dividedBy($two);
                continue;
            }

            // Если a чётное, b нечётное, то НОД(a, b) = НОД(a, b/2);
            if ($a->isOdd() && $b->isEven()) {
                $b = $b->dividedBy($two);
                continue;
            }

            // Если a, b нечётные и a > b, то НОД(a, b) = НОД((a-b)/2, b);
            if ($a->isOdd() && $b->isOdd() && $b->isLessThan($a)) {
                $a = $a->minus($b)->dividedBy($two);
            }

            // Если a, b нечётные и a > b, то НОД(a, b) = НОД((a-b)/2, b);
            if ($a->isOdd() && $b->isOdd() && $a->isLessThan($b)) {
                $b = $b->minus($a)->dividedBy($two);
            }

            if ($a->isNegative() || $b->isNegative()) {
                return false;
            }
        }
        return $a->multipliedBy($two->power($power));
    }
}