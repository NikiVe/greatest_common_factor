<?php


namespace GCF;


use Brick\Math\BigInteger;
use Tester\TestFunc;

/**
 * Class Euclid_2
 * @package GCF
 *
 * Алгоритм Евклида метод остатоков
 */
class Euclid_2 implements TestFunc
{
    public function run(string $values): string
    {
        $arValues = explode(PHP_EOL, $values);

        $a = BigInteger::of($arValues[0]);
        $b = BigInteger::of($arValues[1]);

        while (!$a->isZero() && !$b->isZero()) {
            $b->isLessThan($a) ? $a = $a->mod($b) : $b = $b->mod($a);
        }
        return $a->isZero() ? (string)$b : (string)$a;
    }
}