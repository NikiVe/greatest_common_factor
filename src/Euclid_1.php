<?php


namespace GCF;


use Brick\Math\BigInteger;
use Tester\TestFunc;

/**
 * Class Euclid_1
 * @package GCF
 *
 * Алгоритм Евклида метод вычитаниями
 */
class Euclid_1 implements TestFunc
{
    public function run(string $values): string
    {
        $arValues = explode(PHP_EOL, $values);

        $a = BigInteger::of($arValues[0]);
        $b = BigInteger::of($arValues[1]);

        while (!$a->isEqualTo($b)) {
            $b->isLessThan($a) ? $a = $a->minus($b) : $b = $b->minus($a);
        }
        return (string)$a;
    }
}